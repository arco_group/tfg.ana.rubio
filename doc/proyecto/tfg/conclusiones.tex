\chapter{Conclusiones}
\label{chap:conclusiones}

\noindent
\drop{A}{} continuación se analizará, de forma general, el trabajo realizado 
según los objetivos propuestos al comienzo del proyecto. También se mencionarán 
posibles mejoras y trabajo futuro a tener en cuenta, buscando mejorar el 
sistema desarrollado. 

\section{Objetivos alcanzados}
El objetivo principal de Citisim era el de desarrollar un simulador genérico 
para ciudades inteligentes que, mediante el uso de un sistema de diseño 3D, 
permitiese representar la Smart City a partir de la información obtenida de un 
sistema \acs{GIS} libre para la validación y prueba de determinados servicios, 
sin necesidad de realizar un despliegue real de sensores y actuadores. Pudiendo 
ser utilizado con tres propósitos distintos (monitorización, simulación y 
3D-UI), el sistema construido hace posible controlar qué es lo que está 
sucediendo en la ciudad real, probar el funcionamiento de otros servicios o 
sistemas que actúan sobre dicha ciudad o cambiar el estado de los objetos que 
conforman la Smart City trabajando como interfaz de usuario. Para este proyecto 
el desarrollo ha sido centrado en el manejo de elementos relacionados con el 
tráfico e iluminación de las calles, utilizando para ello objetos como 
farolas, espiras detectoras de paso de vehículos y semáforos cuya posición es 
obtenida, como ya se ha mencionado, de mapas y fuentes libres: 
OpenStreetMap. Para poder implementar la herramienta propuesta fue necesario 
marcar otros objetivos, más específicos dentro del desarrollo. Estos fueron 
cinco, y determinaron el modo de trabajo y la evolución del proyecto:

\begin{description}
 \item \textbf{Representación de la ciudad.} La automatización del proceso por 
el cual se genera el modelo sobre el que trabajar se marcó como uno de los 
objetivos principales del proyecto, ya que la construcción manual de la 
representación de la ciudad haría que Citisim resultase poco práctico. Por 
ello, y gracias a los plugins de Vladimir Elistratov meniconados en el Capítulo 
\ref{chap:proyecto} de este mismo documento, se ha implementado un proceso que 
a partir de los datos que proporciona OpenStreetMap genera cualquier ciudad, 
siempre que exista la información necesaria en los archivos \acs{OSM}. Aunque 
también se consideraron otras formas de generar el modelo (como la descarga de 
mapas en formato imagen para su posterior vectorización y extrusión en 
Blender), el modo en que OpenStreetMap estructura la información era el que más 
se adaptaba a las necesidades del proyecto: la georeferenciación de la escena 
que permiten los archivos \acs{XML} descargables desde la web y el trabajo con 
nodos posicionados geográficamente con una latitud y una longitud hacían más 
sencillo alcanzar el objetivo siguiente.

 \item \textbf{Automatización de diferentes aspectos del modelado.} El objetivo 
principal de este proyecto era el de implementar una herramienta que permitiese 
validar servicios para Smart City, por lo que se hace necesario representar los 
dispositivos desplegados en la ciudad real en el modelo 3D. El despliegue de 
objetos virtuales (farolas, semáforos y espiras en este caso) también había de 
ser un proceso automático, llevando a emplear un lenguaje de modelado que 
permitiese el procesamiento y la representación de los mismos, así como su 
integración en el sistema distribuido para ser operado de forma remota, sin 
trabajo adicional. De la misma forma que se obtienen los datos de OpenStreetMap 
para la representación de los edificios y las calles de la ciudad, las 
coordenadas que ubican a esos objetos en la escena también son sacadas de 
los archivos \acs{OSM} para el posicionamiento de copias de un objeto ya 
modelado en los lugares en que dispositivos de ese tipo han sido desplegados.
 
 \item \textbf{Desarrollo de un middleware de comunicaciones.} Los servicios 
que Citisim pretendía integrar normalmente habían sido tratados de forma 
individual. Dicha integración requería de un middleware de comunicaciones que 
diese soporte a distintas plataformas de sensorización o actuación desplegables 
en la ciudad virtual haciendo que, independientemente del tipo de dispositivo, 
fuese posible su interoperabilidad e interconexión. Esto ha llevado al uso del 
middleware de comunicaciones orientado a objetos ZeroC Ice, para la 
construcción de la aplicación de forma distribuida, y del protocolo de red 
virtual \acs{IDM}, que permite la integración de dispositivos en dominios 
heterogéneos (como en el caso de las Smart Cities). Aunque la implementación se 
realizó en un primer momento utilizando el middleware semántico Dharma, que 
hace posible la búsqueda e instanciación de servicios en base a propiedades 
semánticas, la unión de \acs{IDM} y \acs{DUO} proporcionaba una interfaz de 
menor nivel de abstracción y permitía una adaptabilidad mayor con respecto 
objetivos posteriores, como es el mecanismo de asociación de dispositivos 
reales y virtuales.
 
 \item \textbf{Establecimiento de un mecanismo para la asociación de 
dispositivos.} Conociendo casos como, por ejemplo, el de un ayuntamiento 
que quisiese realizar una estimación ajustada del ahorro energético que 
obtendría por la instalación de farolas que graduasen su intensidad en función 
de la presencia de personas o vehículos, Citisim buscaba (como ya se ha dicho) 
ser una herramienta de uso sencillo dedicada a la prueba de ese tipo de 
servicios. Las situaciones a simular o monitorizar en el modelo, o la toma de 
decisiones a realizar en función del servicio implementado, tenían que poder 
hacerse mediante la representación de los dispositivos reales en el modelo 
3D. Así surgía la necesidad de establecer como objetivo la búsqueda de un 
mecanismo que permitiese la asociación de cada uno de esos dispositivos del 
mundo real con sus réplicas virtuales. El sistema para llevar a cabo dicha 
asociación fue implementado según la forma de funcionar de \acs{IDM}: 
utilizando las identidades de los objetos que emplea el protocolo para la 
identificación de los elementos que participan en las operaciones, y el 
router \acs{IDM} (desplegado en la frontera entre dominios), cuya función es 
realizar la interconexión de los objetos; y la generación de códigos \acs{QR} 
que proporcionen la identidad de cada uno de los objetos virtuales al ser 
seleccionados para, mediante la aplicación Logic 
Wiring\footnote{\url{https://www.youtube.com/watch?v=JYVIMF9AaEU}} (ver 
Figura \ref{fig:logic_wiring}), asociarlos 
al objeto real que representan (o cualquier otro, como en el caso del trabajo 
con maquetas, elementos para pruebas o similares). Aunque la implementación del 
sistema que genera los \acs{QR} hace que el proceso sea más sencillo y visual, 
este podría no resultar práctico cuando se desee trabajar con un número elevado 
de objetos. En este caso, también se ha hecho posible realizar la asociación 
mediante scripts que, si se dispone de la información pertinente, lleven a cabo 
la operación de forma automática.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=.9\textwidth]{logic_wiring.png}
\caption{Interfaz de la aplicación Logic Wiring}
\label{fig:logic_wiring}
\end{center}
\end{figure}

 \item \textbf{Validación de cada una de las tres facetas del sistema 
propuesto.} La comprobación de los dos modos de ejecución propuestos también 
se hacía necesario para asegurar el correcto funcionamiento de la herramienta. 
El sistema permite trabajar en modo monitor o simulador, representando en el 
modelo 3D el estado de los objetos reales asociados a sus representaciones 
virtuales o haciendo posible el cambio de estado de esos elementos reales 
mediante diferentes acciones sobre el modelo, respectivamente. El uso del 
modelo en este último caso es el que demuestra que la aplicación sirve de 
interfaz de usuario, ya que se interactúa con él para operar sobre los 
objetos reales.
\end{description}

Tras el análisis de cómo los objetivos fueron planteados en un principio según 
necesidades reales en el mundo de las ciudades inteligentes y de cada una de 
las soluciones construidas, puede decirse que todos ellos han sido alcanzados 
de forma satisfactoria.

\section{Trabajo Futuro}
A pesar de haber alcanzado todos los objetivos propuestos en un principio, el 
sistema puede ser mejorado en muchos aspectos y existen multitud de 
posibilidades que podrían añadir nuevas funcionalidades. Para presentar este 
proyecto y mostrar su alcance se ha optado por la implementación de tres modos 
de trabajo en una representación sencilla de la ciudad inteligente, basada en 
un conjunto de edificios, calles, semáforos, farolas y espiras. Un modelo 3D 
generado más realista o un mayor número de tipos de objeto (tres tipos en 
estos momentos, que limitan la herramienta) son algunos de esos aspectos 
mejorables o tareas pendientes:

\begin{itemize}
 \item Reimplementación de los plugins empleados en la construcción del modelo 
3D de la Smart City para una mejor adaptación al objetivo de Citisim. Estos 
plugins son utilizados para generar la ciudad, pero tienen otras opciones y 
utilidades que no son empleadas en la herramienta. Esto haría interesante 
implementar la parte del plugin utilizada en la aplicación, haciendo más 
sencilla la instalación del sistema y eliminando la parte del código no útil. 
Sería de interés desarrollar: la parte del plugin de Vladimir Elistratov que 
modela los edificios y el encargado de darle un grosor a las vías, en 
combinación con el primer plugin, para generar las vías en forma de planos de 
forma directa. Además, en ambas implementaciones podrían añadirse nuevas 
opciones, como la de dar un grosor distinto (e incluso asignar colores 
diferentes) a la vía en función de su tipo, ya que OpenStreetMap proporciona ese 
tipo de información, o construir los edificios de la ciudad con su altura real. 
Este último podría realizarse procesando una de las posibles etiquetas que 
pueden ser asignadas a un objeto de tipo \textit{building} en OpenStreetMap que 
hace posible especificar la altura del edificio o, empleando datos de tipo 
\acs{LiDAR}, analizar cuáles pertenecen al área de un edificio y sacar la altura 
según las coordenadas (para saber más ver el Capítulo \ref{chap:antecedentes}). 
Otro aspecto que podría ser interesante es el modelado de áreas, es decir, la 
representación de zonas como parques, agua, límites de terreno o puertos (ver 
Figura \ref{fig:valencia_render}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=.9\textwidth]{valencia_render.png}
\caption{Representación 3D en Blender del puerto de Valencia}
\label{fig:valencia_render}
\end{center}
\end{figure}

 \item En el Capítulo \ref{chap:resultados} se puede ver, en los vídeos del 
grupo de investigación ARCO, que existe la posibilidad de aplicar el sistema 
desarrollado al manejo y control de determinados servicios: la intensidad de 
las farolas en función de la presencia de personas o vehículos, pudiendo ser 
activadas dependiendo de cuáles sean las opciones de paso; o el control del 
cumplimiento de las normas de tráfico, en este caso, mediante la detección de un 
vehículo que se salta un semáforo y del cuál se lleva a cabo un seguimiento 
activando las cámaras que se encuentran en las calles que son direcciones 
posibles para el infractor. El desarrollo funcional de esos servicios, o 
cualquier otro similar, permitiría mostrar mejor cuál es el alcance de la 
herramienta, además de ser aplicable a situaciones del mundo real.

 \item Como se ha visto en capítulos anteriores de este mismo documento, 
Blend4Web permite exportar los modelos 3D generados en Blender y hacerlos 
representables en navegadores de todo tipo. El desarrollo de Citisim para que 
la validación de servicios pudiese realizarse, utilizando esa herramienta, de 
manera online sería un objetivo que, a pesar de ser complejo, aportaría gran 
valor al sistema.

 \item Los materiales asignados actualmente a los elementos que conforman la 
ciudad son básicos, y se usan con el objetivo de diferenciar los objetos unos 
de otros. Mejoras en iluminación, materiales o texturas podrían hacer que los 
modelos 3D generados resultasen más atractivos.

 \item Posible Trabajo Fin de Máster que consiste en el desarrollo de un sistema 
dentro de Citisim que permita la monitorización o simulación (ya no solo de 
elementos que conformen una Smart City) del comportamiento de las personas en la 
ciudad: se podría dar la posibilidad de monitorizar el comportamiento de 
determinados individuos que llevasen algún tipo de dispositivo, sabiendo cuál es 
su ubicación en el modelo 3D; o de simular las acciones de dichos individuos con 
modelos de comportamiento generados a partir de análisis probabilístico o 
similares. Todo ello para validar los servicios de una Smart City en una ciudad 
virtual aún más fiel a la ciudad real representada hasta ahora.
\end{itemize}



% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End: