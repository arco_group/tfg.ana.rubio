\chapter{Animación y Modelado 3D: Conceptos Básicos}
\label{chap:anexo_b}

\drop{S}{i} anteriormente no se ha utilizado ningún software de diseño 3D, los 
términos de modelado, animación o renderizado pueden no ser familiares. En este 
anexo se presenta un rápido repaso de los conceptos básicos de gráficos por 
computador. Los conocimientos abordados en esta sección\footnote{Todo el 
contenido de este anexo ha sido sacado del libro \cite{blender_development}} son 
universales para cualquier aplicación de modelado o animación.

\section{Animación}
En este contexto, la \textit{animación} hace referencia a la técnica de hacer 
cambiar las cosas a lo largo del tiempo. Por ejemplo, la animación puede 
significar el movimiento de un objeto, su deformación o su cambio de color. 
Para llevar a cabo una animación son utilizadas instantáneas en el tiempo, 
conocidas como \textit{snapshots}, que permiten guardar los diferentes estados 
por los que pasa el objeto en los distintos instantes de tiempo. Es el software 
de animación el que se encarga de realizar la transición entre instantáneas, 
dando lugar a las animaciones. Estas animaciones pueden realizarse cambiando 
características del objeto como la posición o el color de forma manual, pero 
también puede animarse un objeto utilizando las físicas que proporcione el 
software de animación.

\section{Modelado}
En gráficos por computador el modelado 3D es entendido como el proceso por el 
cual se construye una representación matemática de cualquier objeto 
tridimensional a través de software especializado.

\subsubsection{Sistema de Coordenadas}
Vivimos en un mundo tridimensional con ancho, altura y profundidad. Esto hace 
que la representación de cualquier objeto del mundo real en el mundo virtual 
dentro de un computador tenga que ser contruida teniendo en cuenta esas tres 
dimensiones. El sistema más utilizado para ello es el de coordenadas 
cartesianas, donde las tres dimensiones son representadas por X, Y y Z, 
dispuestas como planos que se cortan (ver Figura 
\ref{fig:cartesian_coordinates}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=.45\textwidth]{coord_system.jpg}
\caption{Sistema de Coordenadas Cartesianas}
\label{fig:cartesian_coordinates}
\end{center}
\end{figure}

El punto en el que los tres ejes se encuentran es llamado \textit{origen}, que 
puede ser considerado el centro del universo digital. Una posición en dicho 
universo será representada por tres números correspondientes a su posición 
con respecto al origen: \texttt{(2, -4, 8)} será un punto en el espacio que se 
encuentra a dos unidades del origen a lo largo del eje X, 4 unidades en el eje 
Y y 8 unidades en el eje Z.

\subsubsection{Puntos, Aristas, Vértices, Triángulos y Mallas}
Aunque se puede definir la posición de un punto (o \textit{vértice}, como es 
conocido en el ámbito de los gráficos por computador) concreto en el espacio 
utilizando las coordenadas XYZ, no resulta muy útil; es difícil ver un punto 
infinitesimalmente pequeño. Pero es posible unir dos vértices para formar una 
línea (a la que se llama \textit{arista}) que, a su vez, puede ser unida a 
otras líneas con el fin de formar figuras, ya sí más visibles. En la unión de 
tres aristas de forma cíclica formaríamos un triángulo, dando lugar a una 
\textit{cara}. Un conjunto de caras conectadas sería lo que conocemos como 
\textit{modelo} o \textit{malla}(ver Figura ~\ref{fig:vertex_edge_face}). Los 
gráficos por 
computador modernos utilizan el triángulo como la foma básica para la 
construcción de cualquier forma. Un plano rectangular (también conocido como 
\textit{cuadrilátero}) está formado por la colocación de dos triángulos uno al 
lado del otro y, del mismo modo, un cubo es un modelo que se forma por la unión 
de seis cuadrados. Incluso una esfera está formada por caras diminutas 
conectadas.

\begin{figure}[h]
\begin{center}
\includegraphics[width=.45\textwidth]{vertex_edge_face.png}
\caption{Vértices, aristas y caras de un objeto}
\label{fig:vertex_edge_face}
\end{center}
\end{figure}

\begin{quote}
 <<Técnicamente, existen otros enfoques en los gráficos por computador que no 
consisten en triángulos o polígonos, como \acs{NURBS} and \acs{VOXEL}. Pero el 
modelado y renderizado con polígonos es el más común, y es el único método 
soportado por los motores de juego (o \textit{game 
engine}).>>~\cite{blender_development}
\end{quote}

El proceso de crear una malla mediante la colocación de vértices, aristas y 
caras es lo que se define como \textit{modelado}. Es destacable que, a 
diferencia del mundo real, estos modelos poligonales no tienen volumen. Son 
simplemente ``conchas'' formadas por la conexión de caras que toman la forma 
del objeto. Otro concepto que cualquier modelador debe conocer es el de 
\textit{normals} o \textit{normales} (ver Figura~\ref{fig:normals}). Las 
normales son una propiedad de las caras que indica la dirección en la que el 
polígono está mirando. Son utilizadas, por ejemplo, para la 
computación de las sombras de una superficie, haciendo que sea importante el que 
estén dirigidas hacia el exterior del modelo. Una mala orientación de estas 
normales podría dar lugar a caras negras o invisibles.

\begin{figure}
\begin{center}
\includegraphics[width=.45\textwidth]{normals.jpg}
\caption{Representación de las normales de las caras de un objeto}
\label{fig:normals}
\end{center}
\end{figure}

\subsubsection{Transformaciones Básicas}
Las tres transformaciones básicas y más comunes con las que cualquier modelador 
debería estar familiarizado son:

\begin{itemize}
 \item \textbf{Traslación}, el movimiento de un objeto en cualquier dirección.
 \item \textbf{Rotación}, el movimiento de un objeto en torno a un punto.
 \item \textbf{Escalado}, cambio del tamaño de un objeto con respecto a un 
punto.
\end{itemize}

\subsubsection{Materiales y Texturas}
Usando polígonos es como se define la forma de una malla. Para cambiar el color 
o la aparencia de esos polígonos se necesitan aplicar \textit{materiales} a 
dichas formas. Los materiales controlan los colores, el brillo o la 
transparencia de un objeto, sirviendo como una técnica que añade detalles a un 
objeto.

A menudo, el cambio de color no es suficiente para hacer una superficie 
realista, y aquí es donde entran en juego lo que es conocido como 
\textit{texturas}. El texturizado es una técnica común usada para añadir color 
y un nivel de detalle mayor a una malla con una imagen, es decir, la proyección 
de una imagen 2D en una malla en 3D (llamado \textit{mapeo de una textura} o 
\textit{texture mapping}). Una textura es considerada como una extensión de 
un material y es la que cambia el color de una superficie, además de la 
transparencia, la reflexión o el granulado, pudiendo hacer que una superficie 
parezca rugosa. Generalmente, las texturas son archivos de tipo imagen. Pero 
hay otras formas de texturizar una superficie mediante el uso de texturas 
generadas por un algoritmo en tiempo real y que son conocidas como 
\textit{procedural textures}.

\subsubsection{Luces}
Todo lo que vemos es el resultado de la luz proyectada sobre los objetos y, del 
mimso modo, las \textit{luces} son muy importantes en el mundo virtual. Con 
ellas también surgen las \textit{sombras}. Estos dos conceptos son muy 
importantes y marcan la diferencia a la hora de presentar una escena. En la 
mayoría de las aplicaciones 3D hay disponibles diferentes tipos de luz 
(\textit{spot}, \textit{area} u otros).

Es necesario pensar en la iluminación como algo que, más allá de permitirnos 
ver los objetos, hace que podamos presentar la escena de diferentes formas, 
añadiendo drama, realismo o similares.

\subsubsection{Cámaras}
Cuando se crea una escena 3D la vista que se tiene del mundo virtual es 
``omnisciente'', puediendo ser visto y editado desde cualquier ángulo. Una vez 
la ejecución comienza, la vista pasa a funcionar como una cámara real. Ya sea 
para ser utilizadas con motores de juego o en el renderizado de escenas, las 
cámaras permiten determinar qué es lo que va a ver el usuario del mundo 
virtual, pudiendo estar situadas por toda la escena en función de lo que se 
quiera mostrar.

\begin{quotation}
<<Una Cámara es un objeto que provee el medio desde el cual procesar las 
imágenes. Definen qué partes de una escena son visibles en el momento 
del procesamiento.>>\footnote{Blender Wiki: 
\url{https://wiki.blender.org/index.php/Doc:ES/2.6/Manual/Render/Camera}}
\end{quotation}


Las cámaras suelen ser tratadas como un objeto más de la escena. En el caso de 
renderizados, hacen posible definir qué zona de la escena se desea obtener 
para dar lugar a imágenes o vídeos; en videojuegos dan la posibilidad de, por 
ejemplo, representar el movimiento de un personaje.



