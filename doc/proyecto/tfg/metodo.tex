\chapter{Método y Fases de Trabajo}
\label{chap:fases}

\noindent
\drop{C}{on} el objetivo de mostrar al lector cómo ha sido el flujo de trabajo 
durante el desarrollo de Citisim, en este capítulo se intentará dar una 
visión general de la metodología de trabajo empleada de forma teórica y de su 
aplicación en el proyecto. También serán descritas aquí las herramientas 
utilizadas en el proceso, tanto hardware como software.

\section{Metodología}
Para poder llevar a cabo el proyecto se han analizado diferentes 
metodologías ágiles y metodologías tradicionales. Estas últimas, que son 
aquellas que se centran más en la planificación y en el control del proyecto 
además de en una especificación precisa de requisitos y en el modelado, no 
se adaptan adecuadamente a los cambios, haciendo que los métodos empleados 
no sean adecuados cuando se trabaja en un entorno donde los requisitos no 
pueden definirse de forma concreta o pueden variar.

Las metodologías ágiles son la reacción a ese modo de desarrollo del software y 
conocimiento donde el trabajo comienza con la construcción de una 
extensa documentación en la que se recogen todos los requerimientos del 
sistema, seguido de un diseño arquitectural de alto nivel, una completa 
descripción del proceso de desarrollo y otros. A mediados de 1990 algunos 
comenzaron a encontrar este desarrollo inicial frustrante y, en ocasiones, 
imposible debido al continuo cambio de las necesidades del cliente y de la 
industria y la tecnología, dando lugar a nuevos métodos y prácticas que 
intentaban resolver los problemas que por ello surgían 
\cite{cohen_agilesoftware}.

De forma más concreta y según los autores de una de las piezas más importantes 
del movimiento ágil donde se explica cuáles son los valores de los métodos 
ágiles y en qué se distinguen dichos métodos de los tradicionales, el 
Manifiesto por el Desarrollo Ágil de 
Software\footnote{\href{http://www.agilemanifesto.org/iso/es/}{
http://www.agilemanifesto.org/iso/es/}}:

\begin{quote}
<<Estamos descubriendo formas mejores de desarrollar software tanto por 
nuestra propia experiencia como ayudando a terceros. A través de este 
trabajo hemos aprendido a valorar:

  \begin{itemize}
    \item Individuos e interacciones sobre procesos y herramientas.
    \item Software funcionando sobre documentación extensiva.
    \item Colaboración con el cliente sobre negociación contractual.
    \item Respuesta ante el cambio sobre seguir un plan.
  \end{itemize}

Queriendo decir que, aunque valoramos los elementos de la derecha, valoramos 
más los de la izquierda>>.
\end{quote}

Así, debido a la amplitud de los objetivos marcados y a la naturaleza 
del proyecto, se ha optado por una metodología ágil que pueda adaptarse a 
los cambios y proveer respuestas rápidas ante cualquiera de esos cambios u 
otro tipo de problemas. Esta forma de trabajo nos permitirá desarrollar un 
software que en primer lugar se adapte a lo descrito en este mismo 
documento, pero también poder realizar cambios conforme vayan surgiendo 
nuevas necesidades que pueden no haberse tenido en cuenta al principio. Con 
una metodología ágil podremos utilizar diferentes procesos software que, en 
vez de rechazar los cambios propuestos, sean capaces de incorporarlos al 
proyecto. Además, este tipo de metodologías proporcionan una serie de pautas 
y principios junto a ciertas técnicas que pueden no ser útiles en 
determinadas situaciones, pero que hacen la entrega del proyecto menos 
complicada y más satisfactoria para todos los implicados.

\section{Prototipado}
El incremento  de los  costes en  desarrollo software y  el creciente  número de
sistemas que no logran satisfacer las necesidades del cliente una vez terminados
han llevado a  que cada vez más organizaciones estén  recurriendo al trabajo con
prototipos~\cite{operational_prot}. En este ámbito se hace complicado definir el
concepto de \textit{prototipo}, ya que no  puede ser utilizado en su sentido más
estricto. Literalmente,  prototipo significa  <<primer tipo>>, noción  que tiene
sentido en aquellas ramas de la  ingeniería donde el objetivo es producir varios
productos de  un mismo tipo.  Aquí prototipado hace  referencia a una  fase bien
definida del proceso de producción, donde un modelo es construido para presentar
las características esenciales del producto, en  forma de test o como guía, para
acercarse  al diseño  final que  se está  buscando y  que se  ajusta mejor  a lo
solicitado por el cliente~\cite{prototyping_floyd}:

\begin{quote}
 <<Implementación parcial de un sistema construida expresamente par aprender 
más sobre un problema concreto o sobre su solución.>>~\cite{operational_prot}
\end{quote}

El trabajo con prototipos ha sido una práctica generalizada en las industrias 
de ingeniería y fabricación durante décadas: los diseñadores de aviones 
utilizan prototipos para saber más sobre las características de vuelo, por 
ejemplo; o los diseñadores de automóviles, que los construyen para mostrar la 
ergonomía, el comportamiento, o las características de la suspensión. En el 
desarrollo de software un prototipo implementa parte de los requisitos 
recogidos en un primer momento para, mediante la interacción con el usuario, 
conocer los requisitos reales que lleven a diseños 
alternativos~\cite{operational_prot}. Consiste en cuatro pasos:

\begin{enumerate}
 \item \textbf{Selección funcional:} elección de las funciones que el prototipo 
debe exhibir. La selección debe basarse siempre en las tareas de trabajo más
relevantes que pueden servir como modelo para una demostración del sistema 
general. La gama de características ofrecidas por el prototipo no es la misma 
que la del producto final; las funciones implementadas no serán todas, sino 
solo las seleccionadas como más importantes, sin ser implementadas en detalle 
(se omite parte de su función o se simula).
 \item \textbf{Construcción:} proceso con el que se crea el prototipo. El 
esfuerzo debe ser mucho menor que el implicado en el desarrollo del producto 
final, pudiendo no tener en cuenta aspectos tales como la fiabilidad, la 
seguridad de los datos o la eficiencia, a no ser que sean las funcionalidades a 
mostrar en el prototipo.
 \item \textbf{Evaluación:} paso decisivo en el prototipado, ya que determinará 
el desarrollo futuro. La evaluación puede realizarse a nivel de usuario 
individual que trabaja con el sistema, en cuyo caso sería necesario hacer 
especial énfasis en los problemas relacionados con la interfaz, o a nivel de 
cooperación entre usuarios del sistema o usuarios y personas con otro tipo de 
rol, donde también sería necesario considerar los problemas de comunicación. En 
definitiva, en este paso se muestra al usuario final el prototipo generado con 
el objetivo de que dicho usuario dé lugar a la recogida de nuevos requisitos o 
a la modificación de los recogidos con anterioridad para el desarrollo de un 
sistema que se adapte mejor a lo esperado.
 \item \textbf{Uso posterior:} en función de los resultados obtenidos en la 
evaluación del prototipo, este puede servir simplemente como medio de 
aprendizaje y ser desechado o puede ser utilizado total o parcialmente como 
parte del sistema final.
\end{enumerate}

En función de los objetivos a alcanzar, se puede distinguir entre tres tipos de 
prototipado: de exploración, centrado en la especificación de requisitos y 
características deseables del sistema final para valorar las posibles 
alternativas y soluciones; de experimentación, que hace hincapié en la 
determinación de la idoneidad de las soluciones propuestas antes de realizar 
inversiones en implementación; o el evolutivo, que trabaja con la adaptación 
gradual del sistema (mediante prototipos) cambiando los requisitos, que no 
pueden ser definidos al comienzo~\cite{prototyping_floyd}.

\subsection{Prototipado Evolutivo}
El prototipado evolutivo se centra básicamente en los problemas de 
comunicación que surgen entre los desarrolladores de software, que normalmente 
tienen muy poco conocimiento sobre el campo de aplicación, y los usuarios 
potenciales, que no tienen una idea clara de lo que el equipo encargado de la 
implementación puede hacer por ellos. En este tipo de situaciones, una 
demostración práctica de las posibles funciones del sistema sirve de 
catalizador para promover una cooperación creativa entre ambas partes. Este 
enfoque es considerado uno de los tipos de prototipado más poderoso pero, a su 
vez, el más alejado del concepto <<prototipo>>: algunos autores consideran que 
el prototipado evolutivo no debería ser enmarcado dentro de las metodologías de 
prototipado, sino como una metodología que trata con el desarrollo de 
versiones, es decir, una estrategia a seguir que ve el producto como 
una secuencia de versiones, a evaluar y que sirven como prototipo para 
versiones posteriores\cite{prototyping_floyd}. De forma más concreta:

\begin{quote}
 <<El prototipado evolutivo, también conocido como \textit{prototipado de 
sistema}, se encarga de producir un modelo completo y funcional del sistema de 
información a través de la evolución de un prototipo por etapas. Al inicio se 
empieza realizando un modelo del sistema de información que evoluciona, 
mediante un proceso iterativo, hasta llegar al sistema de información 
completo.>>~\cite{cuenca_tesis}
\end{quote}

El prototipado evolutivo es muy utilizado en el Desarrollo Rápido de
Aplicaciones o \acf{RAD} y en las metodologías de
Desarrollo Ágil, y rompe el orden lineal de los pasos a seguir en las 
metodologías tradicionales basándose en ciclos. Dependiendo de la forma en que 
esto se lleve a cabo, se puede distinguir entre dos modos de 
desarrollo~\cite{prototyping_floyd}:

\begin{enumerate}
 \item \textbf{Desarrollo de sistema incremental}. Consiste en el desarrollo 
hasta la solución de forma gradual. Es considerada una 
estrategia de desarrollo a largo plazo, y el diseño de los aspectos técnicos se 
realiza en un proceso de aprendizaje y crecimiento progresivo donde los 
usuarios pueden participar, obteniendo con ello beneficios obvios en lo 
relativo a la comunicación entre usuarios y desarrolladores. Este desarrollo 
incremental es compatible con el modelo de desarrollo de software en fases ya 
que, aunque se centra en la fase de implementación, está basado en un diseño 
global.
 \item \textbf{Desarrollo de sistema evolutivo}. Trabaja con secuencias de 
ciclos formados por un (re-)diseño, una (re-)implementación y una 
(re-)evaluación, dependiendo del número de ciclos a realizar de cada proyecto 
concreto. Este prototipado es utilizado en  entornos dinámicos y cambiantes, 
llevando a dejar de lado la previa recogida del conjunto completo de requisitos 
con el fin de adaptarse a cambios posteriores, impredecibles. Se requiere por 
parte de los desarrolladores y de los usuarios una disposición a abrirse a la 
comunicación y al cambio a un alto grado: los desarrolladores deben aceptar la 
necesidad de revisar sus propios programas en varias ocasiones, lo que implica 
la necesidad de un estilo de trabajo muy disciplinado; y los usuarios, por otra 
parte, deben estar dispuestos a aceptar repetidos cambios en el sistema que 
afectan a su trabajo y que requieren de nuevos procesos de aprendizaje.
\end{enumerate}

\begin{figure}[!h]
\begin{center}
\includegraphics[width=.6\textwidth]{evolutionary_prototyping.png}
\caption{Diagrama de creación de prototipos}
\label{fig:evolutionary_prototyping}
\end{center}
\end{figure}

El prototipado evolutivo es una metodología que comienza con la recogida y 
especificación de requisitos en base a lo solicitado por el cliente para, a 
partir de estos, comenzar con un diseño rápido de lo que será el prototipo a 
presentar (con funcionalidades que cumplan con las especificaciones principales 
señaladas por el usuario final). Una vez se ha diseñado el prototipo se procede 
a su construcción, un desarrollo rápido que permita presentar el modelo cuanto 
antes al cliente en la evaluación. Dicha evaluación permitirá un acercamiento a 
lo que realmente quiere el usuario final, añadiendo o modificando los 
requisitos utilizados para el diseño del prototipo propuesto. Este refinado 
hará posible un rediseño del prototipo para su reconstrucción y nueva 
presentación 
ante el cliente hasta lograr el sistema final que se adapte correctamente a 
sus necesidades (ver Figura \ref{fig:evolutionary_prototyping}).

\section{Desarrollo del Proyecto}
Citisim es un sistema pensado para hacer posible la validación de servicios
para Smart City sin la necesidad de un previo despliegue de sensores y
actuadores en el mundo real que surgió a raíz de proyectos que se están
llevando a
cabo en la actualidad en el entorno de las ciudades inteligentes. Estos
sistemas,
como el de el aparcamiento de bicicletas en Rivas-Vaciamadrid o el ahorro
energético que se busca en Santander mediante la regulación de la intensidad
lumínica de las farolas distribuidas por la ciudad, son algunos de los
servicios que podrían requerir de un sistema que permitiese probar escenarios
ficticios para ver cómo responden los dispositivos o que hiciese posible
monitorizar qué es lo que está ocurriendo en el mundo real si los elementos que
conforman el sistema ya estuviesen desplegdos.

Aunque esos son algunos ejemplos a partir de los cuales nació la idea de 
Citisim, las aplicaciones del sistema son muchas y, por ello, una recogida 
previa de requisitos a seguir de forma estricta se hace un proceso complicado: 
primero porque el cliente explica cuáles son sus necesidades y qué resultados 
desea obtener, pero la mayor parte de las veces sin conocer las capacidades de 
los desarrolladores; y segundo, porque tampoco son conscientes de las 
posibilidades dentro del producto solicitado, según sus características. Esto 
lleva a que, en primer lugar, se realice un análisis del sistema 
deseado, pero sin ser determinante en la entrega final del producto. 
Es decir, permitiendo cambios a lo largo del desarrollo que surjan de la 
propuesta de nuevas ideas, por parte del cliente o de los desarrolladores, o 
por una implementación incorrecta debido a una mala descripción o a errores de 
comprensión con respecto a esos primeros requisitos.

Así, describiendo los resultados a obtener y teniendo en cuenta ejemplos como 
los servicios anteriormente descritos, se realizó un primer análisis de 
requisitos muy general que permitiese al desarrollador aportar ideas a la vez 
que se amoldaba al producto descrito por el cliente, sabiendo cuáles eran 
las necesidades de este (ver Tabla \ref{tab:requeriments}).

\begin{table}[h]
  \centering
  {\small
  \input{tables/requeriments.tex}
  }
  \caption[Primer análisis general de requisitos]
  {Primer análisis general de requisitos}
  \label{tab:requeriments}
\end{table}

Los objetivos expuestos en el Capítulo \ref{chap:objetivos} de este mismo 
documento son los definidos a partir de la primera especificación de requisitos 
intentando cubrir todas las necesidades del cliente y teniendo en cuenta las 
posibilidades en lo que a la implementación se refiere.

\subsection{Prototipos}
La metodología escogida se basa en demostraciones prácticas de las posibles 
funcionalidades del sistema para definir, de forma progresiva, un producto que 
se adapte exactamente a lo que el cliente necesita (aunque el resultado final 
diste de lo indicado en un primer momento). Las versiones presentadas son 
evaluadas por el cliente, y pueden ser desechadas, servir como base para 
versiones posteriores o incluso formar parte del producto final. En este 
proyecto los prototipos o versiones presentadas han sido cuatro:

\begin{description}
 \item \textbf{Modelado 3D de una ciudad.} Buscando solución para el problema 
propuesto de que era necesario disponer de una representación en 3D del modelo 
del escenario con el que se estuviese trabajando de forma automática, 
entendiendo este como una ciudad o un fragmento de la misma, se presentaron dos 
versiones de ciudad virtual. Ambos modelos 3D presentaban características 
diferentes y fueron elaborados de forma manual, por lo que con estos primeros 
prototipos no se mostraban al cliente modos de representación automáticos 
(aunque los dos pudiesen automatizarse), sino cómo quería que fuese modelado el 
escenario. Tras analizar el software de modelado 3D existente en el mercado y 
llegar a la conclusión de que Blender era el que mejor se adaptaba a la 
resolución de los objetivos propuestos, se comenzó con el primer ciclo del 
proceso de desarrollo:

\begin{enumerate}
 \item \textbf{Diseño.} Se decide llevar a cabo el modelado de dos ciudades 
virtuales: una representación texturizada, compuesta por edificios premodelados 
que podrían situarse en la escena en el mismo lugar que ocuparan los edificios 
en el mundo real (ver Figura \ref{fig:prototype1}); y otra representación que, sin texturas 
y con materiales 
básicos, estuviese constituida por edificios y calles con la misma forma que 
los elementos reales (ver Figura \ref{fig:london_model}).
\item \textbf{Construcción.} Las representaciones se construyen manualmente, 
tanto el modelado de los edificios y de las callles como la asignación de 
texturas y materiales, ya que lo que se pretende con estos prototipos es 
determinar cómo se desea que sea construido el escenario de trabajo. El primer 
modelo está compuesto por edificios cuadrados, modelados antes de la 
construcción de la ciudad virtual, que se repiten a lo largo de la escena 
rodeando las vías representadas (el modelo es más atractivo visualmente, pero 
menos realista). El segundo modelo lo forman elementos dibujados a mano (no 
premodelados) que ocupan el mismo lugar que ocuparían los edificios reales y 
que, además, tienen la misma forma. En este último modelo las calles son 
representadas por líneas, mientras que en el otro prototipo son planos (también 
texturizados).

\begin{figure}[h]
\begin{center}
\includegraphics[width=.8\textwidth]{prototype1.png}
\caption{Primer prototipo presentado}
\label{fig:prototype1}
\end{center}
\end{figure}

\item \textbf{Evaluación.} En el análisis realizado con el cliente de las dos 
versiones propuestas, se llega a la conclusión de que es más útil un modelo con 
formas fieles a la realidad, aunque sea más sencillo, ya que el objetivo del 
producto final es el de trabajar con los dispositivos desplegados en la ciudad.
\item \textbf{Refinado.} Se busca la construcción de un modelo conformado por 
los elementos básicos, los edificios y las calles, sin sobrecargas de texturas 
u objetos que no sean de interés (que no tengan que ver con la validación de 
los servicios). Con los resultados obtenidos en la evaluación, se decide 
modelar una ciudad compuesta por elementos de formas más fieles a la realidad, 
es decir, basado en el segundo prototipo propuesto, pero que represente las 
calles como en el primer prototipo: mediante planos, para una mejor visión 
general de la escena (ver Figura \ref{fig:london_model}).
\end{enumerate}

\begin{figure}[h]
\begin{center}
\includegraphics[width=.8\textwidth]{london_model.png}
\caption{Modelo 3D de Londres (Tower Bridge)}
\label{fig:london_model}
\end{center}
\end{figure}

\item \textbf{Automatización del modelado 3D de la ciudad.} Después de haber 
especificado dónde y cómo se representaría la ciudad (en lo que a estética se 
refiere), se hacía necesario dar con algún medio por el cuál llevar a cabo el 
proceso de modelado de forma automática. Aquí, a diferencia del ciclo anterior y 
aunque se tuvieron en cuenta varias posibilidades, se presentó un único 
prototipo:

\begin{enumerate}
 \item \textbf{Diseño.} Se buscaba que, a partir de alguna fuente de 
infromación, se pudiese reproducir la ciudad real de forma virtual sin 
necesidad de intervenir en el proceso. Conociendo el tipo de ciudad a 
modelar, los elementos por los que tenía que estar formada y el objetivo de 
la herramienta, se barajaron dos posibilidades: una representación generada por 
medio de la descarga de imágenes de fuentes como Google o similares para su 
posterior vectorización y extrusión en Blender, construyendo así una ciudad 
virtual bastante fiel a la ciudad real (con aceras, incluso); y otra 
representación obtenida del procesado de los datos proporcionados por algún 
sistema \acs{GIS}. Las limitaciones que la primera opción presentaba y, a su 
vez, el amplio abanico de posibilidades que la segunda opción ofrecía hicieron 
que la construcción del prototipo en esta iteración se basase en la última de 
las ideas propuestas.
\item \textbf{Construcción.} Para  el desarrollo se optó por  utilizar los datos
  proporcionados en los archivos \acs{XML}  que OpenStreetMap pone a disposición
  del usuario. La obtención de los  datos necesarios para la representación y la
  automatización del modelado a partir de  estos fue un proceso sencillo, ya que
  se  lleva a  cabo  por medio  de  un \textit{plugin}  cuyo  autor es  Vladimir
  Elistratov.  Dicho  plugin  procesa  el   archivo  \acs{OSM}  para  sacar  las
  coordenadas  de los  puntos  que conforman  las  vías y  los  edificios de  la
  ciudad. Además, en este prototipo se  añadió la opción de importar los objetos
  de interés para la validación de servicios propuesta en Citisim, que consistía
  en incluir en la  escena espiras para la detección del  paso de vehículos. Las
  coordenadas  de dichos  objetos  también  se encontraban  en  los archivos  de
  OpenStreetMap,  y su  ubicación  en  la escena  se  realizaba  conforme a  las
  coordenadas de los  objetos reales (gracias a la  georeferenciación del modelo
  que se construye en primer lugar).
\item \textbf{Evaluación.} El grado de satisfacción por parte del cliente en 
este ciclo es total, por lo que el prototipo se establece como parte del 
producto final.
\item \textbf{Refinado.} El prototipo propuesto solamente representaba dónde se 
encontraban objetos de un tipo pero, para dar más posibilidades en lo que a 
validación de servicios se refiere, se decide añadir al sistema la 
representación de otros 2 objetos distintos: farolas y semáforos.
\end{enumerate}

\item \textbf{Implementación de la comunicación entre objetos mediante 
Dharma\footnote{Más información en 
\url{https://arcoresearchgroup.wordpress.com/category/dharma/}}.} 
Intentando cumplir con el objetivo marcado de llegar a que los objetos se 
comuniquen, independientemente del tipo que sean, que daría la 
posibilidad de realizar simulaciones o de monitorizar el mundo real, se busca 
un middleware que permita la construcción sin problemas de un mundo heterogéneo 
conformado por elementos de distinta naturaleza:

\begin{enumerate}
 \item \textbf{Diseño.} El middleware orientado objetos elegido sobre el que 
trabajar, debido a las facilidades que ofrece a la hora de construir 
aplicaciones del tipo de Citisim, fue Dharma. Este es 
un middleware semántico basado en \acs{Ice} que permite la construcción de 
servicios haciendo que cualquier sistema externo pueda conocer su manera de 
funcionar utilizando una interfaz común para las comunicaciones. La 
utilización de Dharma llevaría a esa comunicación entre elementos de distinto 
tipo que se busca en la herramienta.
\item \textbf{Construcción.} Para su posterior evaluación, se construyó un 
prototipo en el que se mostraba, utilizando Dharma, cómo uno de los elementos 
del modelo (una espira, en este caso) se comunicaba con otro objeto externo a 
la ciudad virtual al pulsar sobre su representación. Esta acción permitía 
cambiar el estado del objeto virtual y, a su vez, la del objeto real al que 
representaba. Además, si el estado del objeto real cambiaba, su modelo virtual 
también reproducía ese cambio.
\item \textbf{Evaluación.} Aunque la implementación de las comunicaciones en la 
aplicación solamente se había llevado a cabo para un tipo de objeto, el 
prototipo permitía ver que tanto las monitorizaciones como las simulaciones 
eran posibles, además mediante la interacción con el modelo, cumpliendo así con 
la mayor parte de los objetivos propuestos.
\item \textbf{Refinado.} Será necesario implementar las comunicaciones para el 
resto de objetos representados en la escena.
\end{enumerate}

\item \textbf{Implementación de un sistema de comunicaciones más apropiado.} El 
objetivo principal de la presentación de este prototipo era el de mostrar un 
sistema que fuese, prácticamente, el producto final. En este caso los cambios 
realizados venían de la mano de los desarrolladores y del hecho de que los 
objetos representados en los modelos virtuales de Citisim fuesen sensores y 
actuadores básicos. Esto haría interesante la utilización de un sistema para 
las comunicaciones más sencillo que Dharma, que es adecuado para servicios más 
complejos y flexibles. Además, ZeroC \acs{Ice} presentaba algunas 
limitaciones en lo que a transporte se refiere, por lo que también se intentó 
simplificar la conexión que este middleware realizaba junto con Dharma:

\begin{enumerate}
 \item \textbf{Diseño.} Buscando homogeneizar la interacción remota, 
y aprovechando también al máximo las posibilidades de los dispositivos 
desplegados (igual que hacía Dharma pero de forma más sencilla), se decide 
utilizar el modelo de información denominado \acs{DUO} ~\cite{villatesis}. 
Construir un cableado 
virtual que haga posible la integración de los dispositivos, como el que 
permite formar el protocolo de red virtual \acs{IDM} ~\cite{idm_gitbook}, 
llevará a la 
simplificación de la conexión de los distintos tipos de objeto que son de 
interés en el uso de la herramienta y que se desea conseguir en esta versión.
\item \textbf{Construcción.} El prototipo presentado en esta ocasión mostraba 
las mismas funcionalidades que el prototipo anterior, pero también la opción de 
enlazar los objetos de forma manual utilizando códigos QR mediante la 
aplicación Logic Wiring. La unión consistía en el escaneo del código del 
objeto real y el código de su representación virtual.
\item \textbf{Evaluación.} Aunque la forma en la que se establece la conexión 
entre los elementos reales y virtuales resultó atractiva, se estbleció como 
necesidad la implementación de un sistema que enlazase los sensores y 
actuadores de forma automática si se disponía de la información necesaria.
\item \textbf{Refinado.} La implementación de la comunicación para el resto de 
los objetos representados, no solo para las espiras, sería necesaria de cara a 
la entrega del producto final.
\end{enumerate}
\end{description}

Los distintos prototipos entregados definieron cuáles eran las 
necesidades reales del cliente para, así, llevar a cabo la construcción de un 
sistema que cubriese los objetivos propuestos. Todo ello gracias a un método de 
trabajo que permitió la adaptación de la herramienta implementada a los cambios 
propuestos durante el desarrollo, por parte tanto del cliente como por parte de 
los desarrolladores. El producto final es el obtenido tras la realización de 
este \acs{TFG}: una aplicación para Smart Cities que, construida sobre Blender, 
permite la generación automática de modelos 3D de cualquier ciudad a partir de 
las coordenadas geográficas obtenidas de \acs{GIS} libres con la intención de 
llevar a cabo simulaciones y monitorizaciones del mundo real sobre el entorno 
virtual generado y que, sin necesidad de un previo despliegue de sensores y 
actuadores en el mundo real, hace posible a desarrolladores de servicios para 
Smart Cities la validación de los mismos.

\subsection{Herramientas}
A continuación se listarán, y explicarán de forma breve, cuáles han sido las 
herramientas empleadas en el proceso de desarrollo de este proyecto (tanto en 
la implementación como en la escritura de este documento u otros).

\subsubsection{Lenguajes}
Debido a los requerimientos de las herramientas empleadas en el desarrollo, 
como es el caso de Blender, se ha optado por un uso generalizado en la 
implementación del lenguaje de programación 
\textbf{Python}\footnote{\url{https://www.python.org}}. Este lenguaje es 
interpretado, usa tipado dinámico y soporta la orientación a objetos, 
programación imperativa y programación funcional, aunque en menor medida. Es 
administrado por la \textit{Python Software Foundation} y es de código abierto, 
con una licencia denominada \textit{Python Software Foundation License}.
\subsubsection{Gráficos}
\begin{itemize}
 \item \textbf{Blender}, software libre y de código abierto para la creación de 
trabajos en 3D: modelado, \textit{rigging}, animación, simulación, renderizado, 
composición y edición de vídeo y otros. Es empleado en el proyecto para el 
modelado de la ciudad virtual sobre la que realizar la validación de servicios 
para la Smart City.
 \item \textbf{Inkscape}\footnote{\url{https://inkscape.org/es/}}, editor de 
gráficos vectoriales de código abierto, similar a programas como Adobe 
Illustrator, Corel Draw, Freehand o Xara X; pero que utiliza como formato nativo 
el \acf{SVG}, un  estándar abierto de \acf{W3C} basado en \acs{XML}.
 \item \textbf{\acf{GIMP}}\footnote{\url{https://www.gimp.org}}, programa de 
edición de imágenes digitales en forma de mapa de bits. Es un programa libre y 
gratuito que forma parte del proyecto \acs{GNU} y está disponible bajo la 
Licencia pública general de \acs{GNU} y \textit{\acs{GNU} Lesser General Public 
License}.
\end{itemize}

\subsubsection{Hardware}
Para poder llevar a cabo el desarrollo de este proyecto se ha considerado 
necesaria la adquisición de nuevas estaciones de trabajo que puedan soportar el 
renderizado de imágenes de nuestra Smart City y las representaciones en 3D de la 
misma. Así, intentando elegir un equipo que se adapte a las necesidades que 
surgen por los objetivos planteados, en su momento se barajaron diferentes 
posibilidades: equipos de MSI, Apple, Dell y otros con características similares 
(tarjetas gráficas especialmente creadas para el diseño 3D, procesadores de 
última generación y otros). Las estaciones elegidas fueron, finalmente, la 
\textit{Graphite 40}, de Mountain (ver Tabla 
\ref{tab:mountain_characteristics}), y un PC de sobremesa con las 
especificaciones mostradas en la Tabla \ref{tab:PC_characteristics}.

\begin{table}[h]
  \centering
  {\small
  \input{tables/mountain_characteristics.tex}
  }
  \caption[Especificaciones equipo portátil]
  {Especificaciones equipo portátil}
  \label{tab:mountain_characteristics}
\end{table}

\begin{table}[h]
  \centering
  {\small
  \input{tables/PC_characteristics.tex}
  }
  \caption[Especificaciones equipo de sobremesa]
  {Especificaciones equipo de sobremesa}
  \label{tab:PC_characteristics}
\end{table}

\subsubsection{Sistemas Operativos}
Se ha trabajado (o se han realizado 
pruebas con el sistema implementado) en dos sistemas operativos diferentes. 
Aunque fuese Debian\footnote{\url{https://www.debian.org/}} elegido finalmente 
como el \acs{SO} de mejores características para el correcto funcionamiento de 
Citisim, también se consideró la utilización de 
Ubuntu\footnote{\url{http://www.ubuntu.com/}}:
\begin{itemize}
 \item \textbf{Debian}, sistema 
operativo libre con un conjunto de utilidades y programas básicos para el 
correcto funcionamiento de un computador. Es más que un sistema operativo puro, 
con más de 43000 paquetes precompilados y distribuidos en un formato que permite 
una instalación sencilla y sin problemas.
 \item \textbf{Ubuntu}, sistema operativo 
basado en \acs{GNU}/Linux y que se distribuye como software libre, incluyendo su 
propio entorno de escritorio denominado \textit{Unity}. Está orientado al 
usuario promedio, con un fuerte enfoque en la facilidad de uso y en mejorar la 
experiencia del usuario.
\end{itemize}

\subsubsection{Otros}
\begin{itemize}
 \item \textbf{\LaTeX{}}\footnote{\url{https://www.latex-project.org}}, sistema 
de composición de texto de alta calidad que incluye características diseñadas 
para la producción de documentación técnica y científica. LaTeX es el estándar 
de facto para la comunicación y publicación de documentos científicos.
 \item \textbf{Kile}\footnote{\url{http://kile.sourceforge.net}}, editor de 
texto \LaTeX{}  que funciona para entornos gráficos 
KDE\footnote{\url{https://www.kde.org}} en diferentes sistemas operativos. 
Proporciona herramientas para el autocompletado de comandos \LaTeX{}, coloreado 
de sintaxis, trabajo con múltiples ficheros, creación de documentos con 
plantillas y patrones o plegado de código.
 \item \textbf{Git}\footnote{\url{https://git-scm.com}}, software de control de 
versiones diseñado por Linus Torvalds, pensando en la eficiencia y la 
confiabilidad del mantenimiento de versiones de aplicaciones cuando éstas tienen 
un gran número de archivos de código fuente.
 \item \textbf{GNU Emacs}\footnote{\url{https://www.gnu.org/software/emacs}}, 
originalmente \textit{Editor MACroS}, es un editor de texto con una gran 
cantidad de funciones, muy popular entre programadores y usuarios técnicos. Es 
parte del proyecto \acs{GNU} y su manual lo describe como <<un editor 
extensible, personalizable, auto-documentado y de tiempo real.>>

 \item \textbf{Shell} o intérprete de comandos, que consiste en la interfaz de 
usuario tradicional de los sistemas operativos basados en Unix o similares, 
como \acs{GNU}/Linux. Mediante las instrucciones que aporta el intérprete, el 
usuario puede comunicarse con el núcleo y por extensión, ejecutar dichas 
órdenes, así como herramientas que le permiten controlar el funcionamiento de la 
computadora.
 \item \textbf{GNU Make}\footnote{\url{https://www.gnu.org/software/make}}, 
herramienta de gestión de dependencias (típicamente las que existen entre los 
archivos que componen el código fuente de un programa) para dirigir su 
recompilación o "generación" automática.
 \item \textbf{Atom}\footnote{\url{https://atom.io/}}, editor gratuito y de 
código abierto de texto y código fuente para OS X, Linux y Windows con soporte 
para plugins escritos en Node.js, desarrollado por GitHub. Atom es una 
aplicación de escritorio construida utilizando tecnologías web para la que la 
mayoría de los paquetes que se extienden tienen licencias de software libre y 
son mantenidos y construido por la comunidad.
 \item \textbf{Kate}\footnote{\url{https://kate-editor.org/}}, o \textit{KDE 
Advanced Text Editor}, es un 
editor de textos para el entorno de escritorio KDE.
\end{itemize}



% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End: