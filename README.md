# CITISIM #

Citisim es un simulador de Smart City que, construido sobre [Blender](https://www.blender.org/), permitirá la generación automática de un plano 3D de cualquier ciudad a partir de sus coordenadas geográficas. Una vez construido el plano, Citisim puede ser utilizado con un doble propósito:

* **Monitorización de eventos** que ocurran en la Smart City.
* **Simulación de eventos** en la ciudad virtual.

El resultado de esta plataforma será una herramienta que permitirá a desarrolladores de servicios para Smart City la validación de los mismos sin necesidad de un previo despliegue de sensores reales en la ciudad. Por otro lado permitirá también el despliegue virtual de sensores para la validación de servicios que impliquen algún aspecto de monitorización.

## Instalación  (en proceso)##

PACKAGE                | VERSION       
----------------------:| :------------ 
ZeroC Ice              | 3.5.1-5.2   
Python Wrapt           | 1.8.0-5  
Python3 Wrapt          | 1.8.0-5
Python3 Ice Thing      | 0.2016 02 17-1
Propery Service Simple | 0.2016 02 17-1      
Dharma                 | 0.2016 02 17-1
Python Commodity       | Unknown       

En primer lugar, y para llevar a cabo la correcta instalación de los paquetes necesarios en el funcionamiento de Citisim, habremos de añadir el repositorio de paquetes Pike:


```
#!bash

sudo su
echo "deb http://pike.esi.uclm.es/arco sid main" > /etc/apt/sources.list.d/pike.list
apt-get update
apt-get install arco-archive-keyring
```

y el mirror de Zeroc Ice, también de Pike:

```
#!bash

sudo su
echo "deb http://pike.esi.uclm.es:81/zeroc stable main" > /etc/apt/sources.list.d/zeroc.list
apt-get update
```
* __Python 3__
    1. Instalar con:
       `apt-get install python3`

* __ZeroC Ice__
    1. Instalar con: `apt-get install zeroc-ice36` y `apt-get install python3-zeroc-ice36`


* __IDM__
    1. Instalar con:
        `apt-get install idm`


### Preparando Blender ###
Aunque el código de este repositorio se va adaptando a la última versión de Blender disponible es posible que en algún momento esté utilizando versiones anteriores. Por ello se proporcionan dos opciones de instalación: la primera es recomendable siempre y cuando la última versión estable de Blender en el PPA sea la 2.76 (de no ser así saltar a la opción 2).

* __OPCIÓN 1__ *(recomendado si versión = 2.76)*
    1. Añade el [PPA de Thomas Schiex](https://launchpad.net/~thomas-schiex/+archive/ubuntu/blender):
        `sudo add-apt-repository ppa:thomas-schiex/blender`
    2. Actualiza las listas de paquetes:
        `sudo apt-get update`
    3. E instala:
        `sudo apt-get install blender`

* __OPCIÓN 2__
    1. Descarga Blender (versión 2.76) de su [página oficial](http://download.blender.org/release/).
    2. Descomprime la descarga.
    3. Si ya tenías otra versión de Blender instalada, elimínala: 
        `sudo apt-get remove blender`
    4. Mueve la carpeta obtenida al directorio */usr/lib/blender*:
        `sudo cp blender /usr/lib/blender -r`

Para el correcto funcionamiento de CITISIM necesitaremos añadir diferentes plugins a Blender: [Import OpenStreetMap (.osm)](https://github.com/vvoovv/blender-geo/wiki/Import-OpenStreetMap-%28.osm%29) de [Vladimir Elistratov](https://github.com/vvoovv) para la generación de edificios y [Offset Edges](https://github.com/Bombaba/BlenderPythonScripts/blob/master/mesh_offset_edges.py) de [Bombaba (Github)](https://github.com/Bombaba), que permite darle ancho a las calles de la ciudad (más info [AQUÍ](http://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Modeling/offset_edges)). El proceso de instalación y activación es el mismo para ambos plugins:

1. Ejecuta Blender.
2. Navega hasta *File/User Preferences*.
3. En la ventana selecciona *Addons*.
4. Pincha en *Install from File...*
5. Ve hasta el directorio en el que hayas guardado el plugin, selecciona y pincha en *Install from File*. Puesto que se han realizado algunos cambios en los plugin originales es recomendable instalar los proporcionados en este repositorio (directorio *source/src/plugins*).
6. Actívalo marcando la casilla que aparece a la derecha del nombre del plugin *Enable an addon*.![Captura4.PNG](https://bitbucket.org/repo/x6LdKX/images/2332228456-Captura4.PNG)
7. Guarda los cambios pinchando en *Save User Settings* y cierra la ventana.

¡LISTO!