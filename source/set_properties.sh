#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "[!] Info file not specified."
    echo "  |-- Usage: ./set_properties.sh <info_file_path>"
    exit 0
fi

info_file=/tmp/citisim_info   #PARA PRUEBAS
#info_file=$1   UTILIZAR PARA PASAR ARCHIVO COMO ARGUMENTO
if [ ! -f $info_file ]; then
    echo '[!] Info file not found!'
    echo "  |-- Citisim has to be executed first."
    echo "  |-- Check if $info_file exists."
    exit 0
fi

read -r execution_mode < $info_file

if [ $execution_mode = 'simulator' ]; then
  while read line; do
    while read identity object; do
      object_number="${identity: -2}"
      if [ $object = 'loop_detector' ]; then
        prop-tool set "$identity | tags" "['producer']" -t stringseq --Ice.Config=properties_setter.config
        prop-tool set "$identity | name" "loop$object_number" -t string --Ice.Config=properties_setter.config
        prop-tool set "$identity | icon" "automovile" -t string --Ice.Config=properties_setter.config
      elif [ $object = 'street_lamp' ]; then
        prop-tool set "$identity | tags" "['producer']" -t stringseq --Ice.Config=properties_setter.config
        prop-tool set "$identity | name" "light$object_number" -t string --Ice.Config=properties_setter.config
        prop-tool set "$identity | icon" "bulb" -t string --Ice.Config=properties_setter.config
      fi
    done
  done < $info_file
elif [ $execution_mode = 'monitor' ]; then
  while read line; do
    while read identity object; do
      object_number="${identity: -2}"
      if [ $object = 'loop_detector' ]; then
        prop-tool set "$identity | tags" "['consumer']" -t stringseq --Ice.Config=properties_setter.config
        prop-tool set "$identity | name" "loop$object_number" -t string --Ice.Config=properties_setter.config
        prop-tool set "$identity | icon" "automovile" -t string --Ice.Config=properties_setter.config
      elif [ $object = 'street_lamp' ]; then
        prop-tool set "$identity | tags" "['consumer']" -t stringseq --Ice.Config=properties_setter.config
        prop-tool set "$identity | name" "light$object_number" -t string --Ice.Config=properties_setter.config
        prop-tool set "$identity | icon" "bulb" -t string --Ice.Config=properties_setter.config
      fi
    done
  done < $info_file

else
  echo "[!] Execution mode not detected."
  echo "  |-- Execution mode must be 'simulator' or 'monitor'."
  echo "  |-- Check if is specified in $info_file (first line)."
  exit 0
fi

echo "Properties definition DONE!"












# prop-tool set "10:00 | children" "['10:01', '10:02']" -t stringseq
#
# prop-tool set "10:01 | tags" "['producer']" -t stringseq
# prop-tool set "10:01 | name" "button1" -t string
# prop-tool set "10:01 | icon" "touch" -t string
#
# prop-tool set "10:02 | tags" "['consumer']" -t stringseq
# prop-tool set "10:02 | name" "lamp1" -t string
# prop-tool set "10:02 | icon" "bulb" -t string
