#!/bin/bash

if [ "$1" == "--help" ]; then
    echo " Usage: ./get_osm.sh <min_long> <min_lat> <max_long> <max_lat>"
    exit 0
fi

if [ "$1" == "--help" ] || [ "$#" -ne 4 ]; then
    echo "[!] Specify a bounding box as four numbers: left, bottom, right, top."
    echo "  |-- Usage: ./get_osm.sh <min_long> <min_lat> <max_long> <max_lat>"
    echo "  |-- Example: ./get_osm.sh 11.54 48.14 11.543 48.145"
    exit 0
fi

destination="osm_files/downloaded_map.osm"
if [ ! -d "osm_files" ]; then
  mkdir osm_files
fi

wget -O $destination "http://api.openstreetmap.org/api/0.6/map?bbox=$1,$2,$3,$4"
echo "Download COMPLETE!"
