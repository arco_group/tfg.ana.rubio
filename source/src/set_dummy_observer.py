#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('-I /usr/share/slice /usr/share/slice/duo/duo_idm.ice --all')
#Ice.loadSlice('-I /usr/share/slice citisim.ice --all')
import DUO.IDM
#import Citisim
import IDM


class LoopDetectorI(DUO.IDM.IBool.W):
    def __init__(self, sourceid):
        self.observer = None
        self.state = False
        self.sourceid = sourceid

    def set(self, value, source, message, current=None):
        self.state = value
        print("SET VALUE: "+str(value))

    def setObserver(self, observer, current=None):
        self.observer = observer

    def update(self):
        if self.observer is None:
            return

        self.observer.set(self.state)

class LoopDetector(LoopDetectorI):
    def __init__(self, sourceid):
        LoopDetectorI.__init__(self, sourceid)

    def notify(self, state):
        if self.observer is None:
            return

        self.observer.set(state, "")


class Server:
    def __init__(self):
        self.ic = Ice.initialize(sys.argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints("Adapter", "tcp -p 4321")
        self.adapter.activate()
        self.servant_proxy = None
        self.loop_identity_str = sys.argv[1]
        self.dummy_identity_str = sys.argv[2]


    def create_loop_sink(self, loop_id):
        servant = LoopDetector(loop_id)
        self.servant_proxy = self.adapter.add(servant, self.ic.stringToIdentity(self.loop_identity_str))
        self.servant_proxy = DUO.IDM.IBool.WPrx.uncheckedCast(self.servant_proxy).ice_oneway()
        print(self.servant_proxy)


    def set_dummy_observer(self):
        router = self.ic.stringToProxy("1001 -t:tcp -h mauchly.esi.uclm.es -p 6140")
        router = IDM.NeighborDiscovery.ListenerPrx.uncheckedCast(router)
        router.adv(self.ic.proxyToString(self.servant_proxy))

        dummy_proxy = router.ice_identity(self.ic.stringToIdentity(self.dummy_identity_str))
        dummy_proxy = DUO.IDM.Active.WPrx.uncheckedCast(dummy_proxy)

        observer_address = self.identityToAddress(self.loop_identity_str)
        dummy_proxy.setObserver(observer_address)

        self.ic.waitForShutdown()


    def identityToAddress(self, identity_str):
        return int(identity_str, 16).to_bytes(2, byteorder='big')



if __name__ == "__main__":
    server = Server()
    server.create_loop_sink("loop_sink")
    server.set_dummy_observer()
