#!/usr/bin/python -u
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import bpy
import bmesh
import os
import sys
import argparse
from math import pi
from mathutils import Vector
from os.path import abspath

blend_dir = os.path.abspath(bpy.data.filepath)
if blend_dir not in sys.path:
	sys.path.append("src")

from transverse_mercator import TransverseMercator
from import_objects import addObjects, appendQRCamera
from materials import addMaterials, addRouteMaterial
from gen_workspace import genWorkspace
from qr_codes import addQRPlanes, addQRTexture

parser = argparse.ArgumentParser(prog="blender -b -P main.py --", description="Generate a 3D city model, with some objects, to validate Smart City services")


def saveBlend(scene, blend_path):
	scene.game_settings.material_mode = 'GLSL'
	setViewportShading()
	bpy.ops.wm.save_as_mainfile(filepath=blend_path)


def setViewportShading():
	for area in bpy.context.screen.areas: # iterate through areas in current screen
	    if area.type == 'VIEW_3D':
	        for space in area.spaces: # iterate through spaces in current VIEW_3D area
	            if space.type == 'VIEW_3D': # check if space is a 3D view
	                space.viewport_shade = 'MATERIAL' # set the viewport shading to rendered


def cleanScene(scene):
	for ob in scene.objects:
		if ob.type == 'CAMERA' or ob.type == 'LAMP' or ob.type == 'MESH':
			ob.select = True
	bpy.ops.object.delete()


def importOSMFile(osm_file, scene):
	if not os.path.isfile(osm_file):
		print("[!] OSM file not found")
		print(" |-- ")
		parser.print_help()
		sys.exit(0)

	bpy.ops.import_scene.osm(filepath=osm_file,
		ignoreGeoreferencing=False,
		singleMesh=False,
		importBuildings=False,
		importHighways=True,
		thickness=14)

	highways = bpy.context.object
	highways.name = 'highways'

	bpy.ops.import_scene.osm(filepath=osm_file,
		ignoreGeoreferencing=False,
		singleMesh=True,
		importBuildings=True,
		importHighways=False,
		thickness=14)

	buildings = bpy.context.object
	buildings.name = 'buildings'
	return buildings


def scaleCity(scene):
	scale = 1

	for ob in scene.objects:
		if ob.type == 'MESH':
			ob.select = True

			if ob.name == 'buildings':
				scale = getRenderScale(ob.dimensions.xyz)

	bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='MEDIAN')

	con = bpy.context
	obs_sel = con.selected_objects

	if len(obs_sel) >0:
		bpy.ops.transform.resize(
            value=(scale, scale, scale), # this is the transformation X,Y,Z
            constraint_axis=(True, True, True),
            constraint_orientation='GLOBAL',
            mirror=False, proportional='DISABLED',
            proportional_edit_falloff='SMOOTH',
            proportional_size=1)


def getRenderScale(dimensions):
	finalDimensions = Vector((79,60,0))
	axis = 0
	divisor = 1

	if dimensions[0] > finalDimensions[0] or dimensions[1] > finalDimensions[1]:
		if dimensions[0] < dimensions[1]:
			axis = 1

		scale = finalDimensions[axis] / dimensions[axis]
		return scale


def createLamp(name, lamptype, location, scene):
	lamp_data = bpy.data.lamps.new(name=name, type=lamptype)
	lamp = bpy.data.objects.new(name=name, object_data=lamp_data)
	lamp.location = location
	scene.objects.link(lamp)


def addTrackToConstraint(ob, name, target):
    cns = ob.constraints.new('TRACK_TO')
    cns.name = name
    cns.target = target
    cns.track_axis = 'TRACK_NEGATIVE_Z'
    cns.up_axis = 'UP_Y'
    cns.owner_space = 'WORLD'
    cns.target_space = 'WORLD'
    return


def addCamera(origin, target, scene):
	appendQRCamera()
	camera = bpy.data.objects["Camera"]
	QR_plane = bpy.data.objects["QR_plane"]
	camera_data = camera.data

	addTrackToConstraint(camera, 'TrackMiddle', target)
	camera.location = origin
	QR_plane.location = Vector((-0.15048, -0.06388, -0.46309))

	camera_data.clip_start = 0.0
	camera_data.clip_end = 500.0

	# Make this the current camera
	scn = bpy.context.scene
	scn.camera = camera

	for area in bpy.context.screen.areas:
		if area.type == 'VIEW_3D':
			area.spaces[0].region_3d.view_perspective = 'CAMERA'


def renderImages(scene, resolution, renders_directory):
	scene.render.filepath = renders_directory
	scene.render.resolution_percentage = int(resolution)
	bpy.ops.render.render(write_still=True)


def createHighways(highways_hierarchy, scene):
	bpy.ops.object.select_all(action='DESELECT')

	for highway in highways_hierarchy.children:
		highway.select = True
		bpy.context.scene.objects.active = highway

		bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY')
		bpy.ops.object.mode_set(mode='EDIT')

		mesh = bmesh.from_edit_mesh(bpy.context.object.data)
		for vertex in mesh.edges:
			vertex.select = True

		bpy.ops.mesh.offset_edges(geometry_mode='extrude', width=5.3, caches_valid=False)
		bpy.ops.object.origin_set(type='GEOMETRY_ORIGIN')
		bpy.ops.object.mode_set(mode='OBJECT')

		highway.select = False


def addRoute(path, scene):
	bpy.ops.import_scene.osm(filepath=path,
		ignoreGeoreferencing=False,
		singleMesh=False,
		importBuildings=False,
		importHighways=True,
		thickness=14)

	route = bpy.context.object
	route.name = 'route'
	route.location.z = route.location.z+1

	createHighways(bpy.data.objects['route'], scene)


def importObjects(buildings, osm, mode, scene):
	num_objects = 2
	buildings["objects_number"] = num_objects

	num_objects = addObjects("loop_detector", osm, mode, scene, num_objects)
	num_objects = addObjects("traffic_signals", osm, mode, scene, num_objects)
	num_objects = addObjects("street_lamp", osm, mode, scene, num_objects)
	print(str(num_objects-2)+" CREATED")


def parseArgs(args):
	parser.add_argument(
		'-o', '--osm', dest="osm", default="map.osm", help="Map file name", type=str)
	parser.add_argument(
		'-i', '--import-objects', dest="import_objects", default=False, choices=[True, False], help="'True' for import objects, 'False' by default", type=bool)
	parser.add_argument(
		'-m', '--mode', dest="mode", default=None, choices=["simulator", "monitor"], help="Execution mode", type=str)
	parser.add_argument(
		'--resolution', dest="resolution", default=50, help="Render resolution", type=int)
	parser.add_argument(
		'--route', dest="route", default=None, help="OSM with route data")
	parser.add_argument(
		'--output', dest="output_directory", default="city/", help="Directory where save generated files")

	parsed_args, _ = parser.parse_known_args()
	return parsed_args


if __name__ == "__main__":
	sys.argv = sys.argv[sys.argv.index("--") + 0:]
	args = parseArgs(sys.argv)

	genWorkspace(args.output_directory, args.mode)

	scene = bpy.context.scene
	scene.render.engine = 'BLENDER_GAME'
	cleanScene(scene)

	################ 3D MODEL GENERATION ################

	buildings = importOSMFile(args.osm, scene) # Basic City generation (buildings and road paths)
	target = bpy.context.object
	createHighways(bpy.data.objects['highways'], scene) # Road representation (planes)
	addMaterials()

	if args.route is not None: # Route representation if it's given
		addRoute(args.route, scene)
		addRouteMaterial()

	if args.import_objects: # import object models if import_objects is True
		importObjects(buildings, args.osm, args.mode, scene)

	################

	scaleCity(scene)
	addCamera(Vector((50,90,50)), target, scene)
	addQRPlanes()
	createLamp('sun', 'SUN', Vector((0, 0, 8)), scene)

	renderImages(scene, args.resolution, args.output_directory+"/renders")
	bpy.ops.file.pack_all()
	#scene.game_settings.use_auto_start = True
	saveBlend(scene, args.output_directory+"/blends/city_model")



# Creación de cámaras y seguimiento: http://wiki.blender.org/index.php/Dev:2.5/Py/Scripts/Cookbook/Code_snippets/Other_data_types
# Plugin carreteras: http://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Modeling/offset_edges
