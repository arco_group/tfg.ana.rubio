#!/usr/bin/python -u
# -*- mode:python; coding:utf-8; tab-width:4 -*-

# Script para asignación de materiales

import bpy
import os
import pyqrcode
from materials import makeMaterial, setMaterial

CAMERA_PATH = 'objects/camera.blend/'


def addQRPlanes():
	bpy.ops.object.select_all(action='DESELECT')
	camera = bpy.data.objects["Camera"]
	object_type = None

	for child in camera.children:
		if child.name == "QR_plane":
			for obj in bpy.data.objects:
				bpy.ops.object.select_all(action='DESELECT')
				child.select = True
				object_type = obj.name[:-11]

				if object_type == "traffic_signals" or object_type == "loop_detector" or object_type == "street_lamp":
					qr_name = "QR"+obj.name[-11:]

					bpy.ops.object.duplicate(linked=False)
					duplicated_plane = bpy.context.selected_objects[0]
					duplicated_plane.name = qr_name

					createQR(obj['identity'], qr_name)
					addQRTexture(duplicated_plane, obj.name)

		child.select = True
		bpy.ops.object.delete()

		bpy.ops.wm.append(
			directory=CAMERA_PATH+"Text",
			filepath='camera.blend',
			filename='set_render_hide.py',
			link=False)


def createQR(identity, qr_name):
	qr = pyqrcode.create(identity)
	image_name = qr_name+".png"
	qr.png("city/qr_images/"+image_name, scale=12)


def addQRTexture(duplicated_plane, object_name):
	image = None
	qr_name = duplicated_plane.name
	image_file = qr_name+".png"

	material = makeMaterial(qr_name, (0.8,0.8,0.8), (0,0,0), 1)
	setMaterial(duplicated_plane, material)

	qr_texture = bpy.data.textures.new(qr_name, type = 'IMAGE')

	try:
		realpath = os.path.dirname(os.path.abspath(__file__))
		realpath += "/../city/qr_images/"+image_file
		image = bpy.data.images.load(realpath)
	except Exception:
		print("[!] Object "+object_name+" doesn't have a QR image.")

	if image:
		qr_texture.image = image
	else:
		try:
			realpath = os.path.dirname(os.path.abspath(__file__))
			realpath += "/../qr_codes/default_qr_code.png"
			image = bpy.data.images.load(realpath)
			qr_texture.image = image
		except Exception:
			print("[!] Default QR image not found.")

	texture_slot = material.texture_slots.add()
	texture_slot.texture = qr_texture
	texture_slot.texture_coords = 'OBJECT'



#Source: http://wiki.blender.org/index.php/Dev:Py/Scripts/Cookbook/Code_snippets/Materials_and_textures
