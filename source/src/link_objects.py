#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('-I /usr/share/slice /usr/share/slice/duo/duo_idm.ice --all')
#Ice.loadSlice('-I /usr/share/slice citisim.ice --all')
import DUO.IDM
#import Citisim
import IDM
import argparse

class Linker:
    def __init__(self, args):
        self.args = self.parse_args(args)
        self.mode = self.args.mode

    def link_objects(self):
        if self.mode == "simulator":
            link_simulator_objects()
        elif self.mode == "monitor":
            link_monitor_objects()
        else:
            print("[!] -- Wrong mode: "+self.mode+"\n |---- Use [-h] option")


    def link_monitor_objects(self):
        pass


    def link_simulator_objects(self):
        pass



    def parse_args(self, args):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "--mode", dest="mode", default="simulation", help="Execution mode (simulator or monitor)", type=str)

        args, _ = parser.parse_known_args()
        return args


if __name__ == "__main__":
    linker = Linker(sys.argv)
    linker.link_objects()
