#!/usr/bin/python -u
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import bpy
import xml.etree.ElementTree as ET
import math
import os
from transverse_mercator import TransverseMercator
from materials import addQRTexture, renameMaterials

SIMULATOR_OBJECTS_PATH = 'objects/simulator.blend/'
MONITOR_OBJECTS_PATH = 'objects/monitor.blend/'
CAMERA_PATH = 'objects/camera.blend/'

def addObjects(object_type, data, execution_mode, scene, object_num):
	tree = ET.parse(data)

	for node in tree.iterfind('node'):
		tag = None
		direction = 0.0
		if node.find('tag[@k="highway"][@v="'+object_type+'"]') is not None:
			name = object_type+"_"+node.attrib['id']
			lat = node.attrib['lat']
			lon = node.attrib['lon']
			tag = node.find('tag[@k="direction"]')

			if tag is not None:
				direction = tag.attrib['v']

			appendObject(scene, name, lat, lon, direction, object_type, execution_mode, object_num)
			object_num += 1

	appendScript(object_type+'_simulation.py', execution_mode)
	appendScript(object_type+'_monitorization.py', execution_mode)
	appendScript(object_type+'_presence_source.py', execution_mode)
	return object_num


def appendScript(script_name, execution_mode):
	if execution_mode == "monitor":
		directory = MONITOR_OBJECTS_PATH
		filepath = 'monitor.blend'
	elif execution_mode == "simulator":
		directory = SIMULATOR_OBJECTS_PATH
		filepath = 'simulator.blend'
	else:
		print("You must specify the execution mode.")
		return

	bpy.ops.wm.append(
		directory=directory+"Text",
		filepath=filepath,
		filename=script_name,
		link=False)


def appendQRCamera():
	bpy.ops.wm.append(
		directory=CAMERA_PATH+"Object",
		filepath='camera.blend',
		filename='Camera',
		link=False)

	bpy.ops.wm.append(
		directory=CAMERA_PATH+"Object",
		filepath='camera.blend',
		filename='QR_plane',
		link=False)

	camera = bpy.data.objects["Camera"]
	qr_plane = bpy.data.objects["QR_plane"]
	qr_plane.parent = camera


def appendObject(scene, name, lat, lon, direction, object_name, execution_mode, object_num):
	ob = None
	identity_string = '60'+'{:02}'.format(object_num)

	if execution_mode == "monitor":
		directory = MONITOR_OBJECTS_PATH
		filepath = 'monitor.blend'
	elif execution_mode == "simulator":
		directory = SIMULATOR_OBJECTS_PATH
		filepath = 'simulator.blend'

	bpy.ops.wm.append(
		directory=directory+"Object",
		filepath=filepath,
		filename=object_name,
		link=False)

	projection = TransverseMercator(lat=scene["latitude"], lon=scene["longitude"])
	(x, y) = projection.fromGeographic(float(lat), float(lon))
	ob = bpy.data.objects[object_name]
	ob.name = name
	direction = -(float(direction)*(math.pi/180.0))
	ob.rotation_euler[2] = direction
	ob.location = (x,y,0)
	ob['identity'] = identity_string

	renameMaterials(ob, object_name, name)
