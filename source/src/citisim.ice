#include <duo/duo_idm.ice>

module Citisim {
  interface BoolActive extends DUO::IDM::IBool::W, DUO::IDM::Active::W {};
};
