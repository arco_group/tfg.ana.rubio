#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('-I /usr/share/slice /usr/share/slice/duo/duo_idm.ice --all')
Ice.loadSlice('-I /usr/share/slice citisim.ice --all')
import DUO.IDM
import Citisim
import IDM


class Client:
    def __init__(self):
        self.ic = Ice.initialize(sys.argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints("Adapter", "tcp -p 4321")
        self.adapter.activate()

    def run(self):
        router = self.ic.stringToProxy("1001 -t:tcp -h mauchly.esi.uclm.es -p 6140")
        router = IDM.NeighborDiscovery.ListenerPrx.uncheckedCast(router)

        proxy = router.ice_identity(self.ic.stringToIdentity(sys.argv[1]))
        print proxy
        light = DUO.IDM.IBool.WPrx.uncheckedCast(proxy)

        if not light:
            raise RuntimeError('Invalid proxy')

        if sys.argv[2] == "True":
            light.set(True, "")
            print "Mandando TRUE"
        else:
            light.set(False, "")
            print "Mandando FALSE"

        self.ic.destroy()
        return 0


if __name__ == "__main__":
    client = Client()
    client.run()
