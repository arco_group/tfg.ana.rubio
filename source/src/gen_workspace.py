#!/usr/bin/python -u
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import sys
import os

def genWorkspace(output_directory, mode):
	if not os.path.isdir(output_directory):
		os.makedirs(output_directory)

	createDirectory(output_directory+"/renders")
	createDirectory(output_directory+"/blends")
	createDirectory(output_directory+"/qr_images")

	info_file_path = output_directory+"/citisim_info"
	if os.path.isfile(info_file_path):
		os.remove(info_file_path)
	info_file = open(info_file_path, 'w')
	info_file.write(mode+"\n")
	info_file.close()


def createDirectory(directory):
	if not os.path.isdir(directory):
		os.makedirs(directory)
	else:
		for root, dirs, files in os.walk(directory, topdown=False):
			for file_name in files:
				os.remove(os.path.join(root, file_name))
			for dir_name in dirs:
				os.remove(os.path.join(root, dir_name))


if __name__ == "__main__":
    genWorkspace(sys.argv[1], "mode")
