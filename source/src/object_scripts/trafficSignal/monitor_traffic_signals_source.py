#!/usr/bin/env python
# -*- coding: utf-8; mode: python; -*-

import sys
import Ice
Ice.loadSlice("-I/usr/share/slice /usr/share/slice/dharma/dharma.ice --all")
import Semantic
import libthing
import shelve

servants = {}


class ObservableService(Semantic.Service):
    def __init__(self):
        self.observer = None

    def performAction(self, action, properties, sink):
        if action == 'set-observer':
            self.observer = sink


class TrafficSignal(ObservableService):
    def __init__(self, sourceid):
        ObservableService.__init__(self)
        self.sourceid = sourceid

    def notify(self):
        if self.observer is None:
            return
        
        self.observer.report(self.sourceid, "set-color", libthing.from_bool(True), {})



class Simulator:
    def __init__(self):
        self.ic = Ice.initialize(sys.argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints("Adapter", "tcp")
        self.adapter.activate()

    def create_traffic_signal(self, traffic_signal_id):
        servant = TrafficSignal(traffic_signal_id)
        traffic_signal = self.adapter.addWithUUID(servant)
        traffic_signal = Semantic.ServicePrx.uncheckedCast(traffic_signal)
        
        servants[traffic_signal_id] = servant


    def set_observer(self, identity, servant_proxy):
    	servant = servants[identity]
    	proxy = self.ic.stringToProxy(servant_proxy)
    	proxy = Semantic.EventSinkPrx.uncheckedCast(proxy)
    	if proxy is None:
    		print("invalid proxy {}".format(str_proxy))
    		return

    	servant.performAction("set-observer", {}, proxy)

    
    # BIG FAKE
    # def set_observer(self, traffic_signal_id):
    #     observer = self.ic.stringToProxy('TEST -t -e 1.1:tcp -h 161.67.106.99 -p 55665')
    #     observer = Semantic.EventSinkPrx.checkedCast(observer)
    #     self.servants[traffic_signal_id].performAction("set-observer", {}, observer, None)

    def notify(self, traffic_signal_id):
        try:
            servant = servants[traffic_signal_id]
        except Exception:
            print("[!] - ERROR: Object ID doesn't exist\n")
            sys.exit(1)
        
        servant.notify()



if __name__ == '__main__':
    simulator = Simulator()
    traffic_signals = shelve.open("/tmp/citisim-proxies")

    try:
        traffic_signal_id = sys.argv[1]
    except Exception:
        print("usage: python3 monitor_traffic_signal_source.py <object_id>\n")
        sys.exit(1)


    for identity, str_proxy in traffic_signals.items():
    	simulator.create_traffic_signal(identity)
    	simulator.set_observer(identity, str_proxy)

    simulator.notify(traffic_signal_id)




# Utilizado en source/objects/traffic.blend.