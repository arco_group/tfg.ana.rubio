import bge
import bpy
import sys
import shelve
import Ice


def set_color(traffic_light_id):
    object = bpy.data.objects[traffic_light_id]

    green_material = object.data.materials['green_material_'+traffic_light_id]
    yellow_material = object.data.materials['yellow_material_'+traffic_light_id]
    red_material = object.data.materials['red_material_'+traffic_light_id]

    scene_object = bge.logic.getCurrentScene().objects[traffic_light_id]
    current_color = scene_object['current_color']

    if scene_object['current_color'] == 'GREEN':
        green_material.emit = 0.01
        yellow_material.emit = 100.0
        scene_object['current_color'] = 'YELLOW'
    elif scene_object['current_color'] == 'YELLOW':
        yellow_material.emit = 0.01
        red_material.emit = 100.0
        scene_object['current_color'] = 'RED'
    else:
        red_material.emit = 0.01
        green_material.emit = 100.0
        scene_object['current_color'] = 'GREEN'


def something():
    pass


def on_traffic_signal_ready():
    owner = bge.logic.getCurrentController().owner
