import bge, bpy
from threading import Thread



def set_color():
    controller = bge.logic.getCurrentController()
    owner = controller.owner
    owner_object = bpy.data.objects[str(owner)]
    mouse_over = controller.sensors['mouse_over']
    mouse_button = controller.sensors['mouse_button']

    green_material = owner_object.data.materials['green_material_'+str(owner)]
    yellow_material = owner_object.data.materials['yellow_material_'+str(owner)]
    red_material = owner_object.data.materials['red_material_'+str(owner)]
    current_color = owner['current_color']

    if mouse_over.positive and mouse_button.positive:
        if owner['current_color'] == 'GREEN':
            green_material.emit = 0.01
            yellow_material.emit = 100.0
            owner['current_color'] = 'YELLOW'
        elif owner['current_color'] == 'YELLOW':
            yellow_material.emit = 0.01
            red_material.emit = 100.0
            owner['current_color'] = 'RED'
        else:
            red_material.emit = 0.01
            green_material.emit = 100.0
            owner['current_color'] = 'GREEN'

        simulator.notify(str(controller.owner))


def on_traffic_light_ready():
    owner = bge.logic.getCurrentController().owner
