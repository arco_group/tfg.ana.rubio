import bge
import bpy
import sys
import Ice
Ice.loadSlice('-I /usr/share/slice /usr/share/slice/duo/duo_idm.ice --all')
Ice.loadSlice('-I /usr/share/slice src/citisim.ice --all')
import DUO.IDM
import Citisim
import IDM

bge.render.showMouse(True)


class LoopDetector(Citisim.BoolActive):
    def __init__(self, sourceid):
        self.observer = None
        self.state = False
        self.sourceid = sourceid

    def set(self, value, source, message, current=None):
        self.state = value
        set_color(self.sourceid, self.state)

    def setObserver(self, address, current=None):
        address = self.addressToString(address)
        self.observer = self.router.ice_identity(Ice.stringToIdentity(address))
        self.observer = DUO.IDM.IBool.WPrx.uncheckedCast(self.observer)

    def update(self):
        self.state = state
        if self.observer is None:
            return

        print(self.observer)
        self.observer.set(self.state, "")


class Server:
    def __init__(self):
        self.ic = Ice.initialize(sys.argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints("Adapter", "tcp")
        self.adapter.activate()
        self.router = self.get_router()

    def save_info(self, identity):
        info_file = open('/tmp/citisim_info', 'a')
        info_file.write(identity+"\tstreet_lamp\n")
        info_file.close()


    def create_loop_sink(self, loop_id, object_num):
        identity_string = '60'+'{:02}'.format(object_num)

        servant = LoopDetector(loop_id)
        loopDetector = self.adapter.add(servant, self.ic.stringToIdentity(identity_string))
        loopDetector = Citisim.BoolActivePrx.uncheckedCast(loopDetector)
        print(loopDetector)

        self.router.adv(self.ic.proxyToString(loopDetector))
        self.save_info(identity_string)


    def get_router(self):
        router = self.ic.stringToProxy("1001 -t:tcp -h mauchly.esi.uclm.es -p 6140")
        router = IDM.NeighborDiscovery.ListenerPrx.uncheckedCast(router)
        return router



server = Server()

def set_color(loop_id, loop_state):
    loop_object = bpy.data.objects[loop_id]
    loop_material = loop_object.data.materials['light_'+loop_id]

    if loop_state is False:
        loop_material.emit = 0.01
    else:
        loop_material.emit = 100.0


def something():
    pass


def on_loop_detector_ready():
    owner = bge.logic.getCurrentController().owner
    object_num = bpy.data.objects['buildings']["objects_number"]
    server.create_loop_sink(str(owner), object_num)
    bpy.data.objects['buildings']["objects_number"] += 1
