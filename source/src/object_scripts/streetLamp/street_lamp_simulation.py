import bge
import bpy
from street_lamp_presence_source import Simulator
import sys
from time import sleep


print("Espiras activas")
simulator = Simulator()

def notifyCollision():
    controller = bge.logic.getCurrentController()
    owner = controller.owner
    collision_sensor = controller.sensors['collision']
    mouse_over = controller.sensors['mouse_over']
    mouse_button = controller.sensors['mouse_button']

    if mouse_over.positive and mouse_button.positive or collision_sensor.positive:
        if owner['current_state'] is False:
            set_state(owner, True)
            owner['current_state'] = True
        else:
            set_state(owner, False)
            owner['current_state'] = False

def set_state(owner, loop_state):
    simulator.servants[str(owner)].update(loop_state)
    set_color(str(owner), loop_state)


def set_color(loop_id, loop_state):
    loop_object = bpy.data.objects[loop_id]
    loop_material = loop_object.data.materials['light_'+loop_id]

    if loop_state is False:
        loop_material.emit = 0.01
    else:
        loop_material.emit = 100.0


def on_loop_detector_ready():
    owner = bge.logic.getCurrentController().owner
    object_num = bpy.data.objects['buildings']["objects_number"]
    simulator.create_loop_detector(str(owner), object_num)
    bpy.data.objects['buildings']["objects_number"] += 1
