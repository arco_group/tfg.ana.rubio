#!/usr/bin/env python3 -u
# -*- coding: utf-8; mode: python; -*-

import sys
import Ice
Ice.loadSlice('-I /usr/share/slice /usr/share/slice/duo/duo_idm.ice --all')
Ice.loadSlice('-I /usr/share/slice src/citisim.ice --all')
import DUO.IDM
import Citisim
import IDM


class LoopDetector(Citisim.BoolActive):
    def __init__(self, router):
        self.observer = None
        self.state = False
        self.router = router

    def set(self, value, source, message, current=None):
        self.state = value

    def setObserver(self, address, current=None):
        address = self.addressToString(address)
        self.observer = self.router.ice_identity(Ice.stringToIdentity(address))
        self.observer = DUO.IDM.IBool.WPrx.uncheckedCast(self.observer)

    def update(self, state):
        self.state = state
        if self.observer is None:
            return

        self.observer.set(self.state, "")

    def addressToString(self, address):
        return "".join("{:02X}".format(c) for c in address)


class Simulator:
    def __init__(self):
        self.ic = Ice.initialize(sys.argv)
        self.adapter = self.ic.createObjectAdapterWithEndpoints("Adapter", "tcp")
        self.adapter.activate()
        self.servants = {}
        self.servant_proxies = {}
        self.router = self.get_router()


    def save_info(self, identity):
        info_file = open('/tmp/citisim_info', 'a')
        info_file.write(identity+"\tstreet_lamp\n")
        info_file.close()


    def create_loop_detector(self, loop_id, object_num):
        identity_string = '60'+'{:02}'.format(object_num)

        servant = LoopDetector(self.router)
        loopDetector = self.adapter.add(servant, self.ic.stringToIdentity(identity_string))
        loopDetector = Citisim.BoolActivePrx.uncheckedCast(loopDetector)
        print(loopDetector)

        self.servants[loop_id] = servant
        self.servant_proxies[loop_id] = loopDetector
        self.router.adv(self.ic.proxyToString(loopDetector))
        self.save_info(identity_string)


    def get_router(self):
        router = self.ic.stringToProxy("1001 -t:tcp -h mauchly.esi.uclm.es -p 6140")
        router = IDM.NeighborDiscovery.ListenerPrx.uncheckedCast(router)
        return router


# Utilizado en source/objects/traffic.blend.
