#!/usr/bin/env python
# -*- coding: utf-8; mode: python; -*-

import sys
import shelve
import Ice
Ice.loadSlice('-I /usr/share/slice /usr/share/slice/duo/duo_idm.ice --all')
import DUO.IDM


class Server(Ice.Application):
    def run(self, args):
        ic = self.communicator()
        sink = ic.stringToProxy(args[1])

        print("Service ready: '{}'".format(sink))

        objects = shelve.open('/tmp/citisim-proxies')
        for identity, str_proxy in objects.items():
            proxy = ic.stringToProxy(str_proxy)
            proxy = DUO.IDM.IBool.WPrx.uncheckedCast(proxy)
            if proxy is None:
                print("invalid proxy {}".format(str_proxy))
                continue

            proxy.setObserver(sink)


if __name__ == '__main__':
    Server().main(sys.argv)

# Utilizado en source/objects/traffic.blend.
