import bge
import bpy
from loop_detector_presence_source import Simulator
import sys
from time import sleep


print("Espiras activas")
simulator = Simulator()

def notifyCollision():
    controller = bge.logic.getCurrentController()
    owner = controller.owner
    collision_sensor = controller.sensors['collision']
    mouse_over = controller.sensors['mouse_over']
    mouse_button = controller.sensors['mouse_button']

    if mouse_over.positive and mouse_button.positive or collision_sensor.positive:
        set_state(owner, True)
        owner['current_state'] = True

    if not mouse_button.positive and owner['current_state'] is not False:
        set_state(owner, False)
        owner['current_state'] = False

def set_state(owner, loop_state):
    simulator.servants[str(owner)].update(loop_state)
    set_color(str(owner), loop_state)


def set_color(loop_id, loop_state):
    loop_object = bpy.data.objects[loop_id]
    loop_material = loop_object.data.materials['loop_material_'+loop_id]

    if loop_state is False:
        loop_material.diffuse_color = (0.000,0.210,0.800)
    else:
        loop_material.diffuse_color = (0.800,0.000,0.004)


def on_loop_detector_ready():
    owner = bge.logic.getCurrentController().owner
    object_num = bpy.data.objects['buildings']["objects_number"]
    simulator.create_loop_detector(str(owner), object_num)
    bpy.data.objects['buildings']["objects_number"] += 1
