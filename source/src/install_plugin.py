#!/usr/bin/python -u
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import bpy
import os
import sys

if __name__ == "__main__":
	sys.argv = sys.argv[sys.argv.index("--") + 1:]

	bpy.ops.wm.addon_install(overwrite=True,
	 	target='DEFAULT',
	 	filepath=sys.argv[0],
	 	filter_folder=True,
	 	filter_python=True,
	 	filter_glob="*.py")

	bpy.ops.wm.addon_enable(module="io_import_scene_osm.py")
#EJECUCIÓN: blender -b -P install_plugin.py -- plugins/io_import_scene_osm.py
