#!/usr/bin/python -u
# -*- mode:python; coding:utf-8; tab-width:4 -*-

# Script para asignación de materiales

import bpy
import os

def makeMaterial(name, diffuse, specular, alpha):
	material = bpy.data.materials.new(name)
	material.diffuse_color = diffuse
	material.diffuse_shader = 'LAMBERT'
	material.diffuse_intensity = 1.0
	material.specular_color = specular
	material.specular_shader = 'COOKTORR'
	material.specular_intensity = 0.5
	material.alpha = alpha
	material.ambient = 1
	material.game_settings.use_backface_culling = False
	return material

def setMaterial(ob, mat):
	me = ob.data
	me.materials.append(mat)

def addMaterials():
	buildings_material = makeMaterial('buildings', (0.593,0.593,0.593), (0,0,0), 1)

	bpy.ops.object.select_all(action='DESELECT')
	buildings_object = bpy.data.objects['buildings']
	buildings_object.select = True
	bpy.context.scene.objects.active = buildings_object

	setMaterial(bpy.context.object, buildings_material)

	highways_material = makeMaterial('highways', (0.008,0.008,0.008), (0,0,0), 1)

	bpy.ops.object.select_all(action='DESELECT')
	highways_hierarchy = bpy.data.objects['highways']

	for highway in highways_hierarchy.children:
		highway.select = True
		bpy.context.scene.objects.active = highway
		setMaterial(bpy.context.object, highways_material)


def addRouteMaterial():
	route_material = makeMaterial('route', (0.000,0.560,0.003), (0,0,0), 1)

	bpy.ops.object.select_all(action='DESELECT')
	routes_hierarchy = bpy.data.objects['route']

	for route in routes_hierarchy.children:
		route.select = True
		bpy.context.scene.objects.active = route
		setMaterial(bpy.context.object, route_material)


def addQRTexture(duplicated_plane, object_name):
	image = None
	qr_name = duplicated_plane.name
	image_file = qr_name+".png"

	material = makeMaterial(qr_name, (0.8,0.8,0.8), (0,0,0), 1)
	setMaterial(duplicated_plane, material)

	qr_texture = bpy.data.textures.new(qr_name, type = 'IMAGE')

	try:
		realpath = os.path.dirname(os.path.abspath(__file__))
		realpath += "/../city/qr_images/"+image_file
		image = bpy.data.images.load(realpath)
	except Exception:
		print("[!] Object "+object_name+" doesn't have a QR image.")

	if image:
		qr_texture.image = image
	else:
		try:
			realpath = os.path.dirname(os.path.abspath(__file__))
			realpath += "/../qr_codes/default_qr_code.png"
			image = bpy.data.images.load(realpath)
			qr_texture.image = image
		except Exception:
			print("[!] Default QR image not found.")

	texture_slot = material.texture_slots.add()
	texture_slot.texture = qr_texture
	texture_slot.texture_coords = 'OBJECT'


def renameMaterials(ob, object_name, name):
	if object_name == 'traffic_signals':
		ob.data.materials['green_material_traffic_signal'].name = 'green_material_'+name
		ob.data.materials['yellow_material_traffic_signal'].name = 'yellow_material_'+name
		ob.data.materials['red_material_traffic_signal'].name = 'red_material_'+name
	elif object_name == 'loop_detector':
		ob.data.materials['blue_material_loop_detector'].name = 'loop_material_'+name
	elif object_name == 'street_lamp':
		ob.data.materials['light_street_lamp'].name = 'light_'+name



#Source: http://wiki.blender.org/index.php/Dev:Py/Scripts/Cookbook/Code_snippets/Materials_and_textures
